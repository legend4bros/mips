# GIT FLOW
------
There are three roles in our team: LORD, DESIGNER, TESTER. Each one is D as well as T, and the routine is as followed:

## Designer

*WARNING*: you can only edit your own branch and may not touch other's.

### begin task (ie. design alu in datapath)

1. create a new branch based on branch:develop

    	git checkout develop
    	git pull origin develop --rebase
    	git checkout -b datapath/alu

2. create new files and edit them

		touch alu.sv
		edit alu.sv with sublime text

3. when you achieve a milestone, you can commit it to your local repo

		git add alu.sv
		git commit -m "what is new?"

4. when you finish your work today OR you need your tester test your code, you should push your branch

		git push origin datapath/alu

### end task

1. after your feature has passed test, then you can merge your feature branch into branch:develop

		git checkout develop
		git pull origin develop
		git merge datapath/alu --no-ff

2. push

		git push origin develop

## Tester

*WARNING*: you can only edit your test branch based on the branch you test.

1. pull the feature branch you want to test

		git pull origin datapath/alu
		git checkout -b datapath/alu_test

2. test

		touch alu_test.sv
		test alu.sv with modelsim

3. push test result

		git push origin datapath/alu_test

4. communicate with designer

# Coding Style
------

## FILE

1. One file contains *ONLY* one module, and both share the same name.

2. At the beginning of each file, following infomation should be added:

* file name

* project name

* top level or father module

* module discription

* recent changes

* author

* version and release date

## NAMING STYLE

1. lower-case is preferred, whereas *CONSTANT* and *PARAMETER* are upper-case.

2. words are separated by `_`

3. meaningful name, such as `address` instead of `a`

4. abbreviate long word to keep name less than 13 chars, for instance, use `addr` instead of `address`

5. during the entire design process, the same variable should keep the same name when module is instantiated.

   		block1  u_block1(
	       .rst_n (rst_n),
	       .clk (clk),
	       .addr (addr),
	       .wr_data (wr_data),
	       ....................
	       );

## PARAMETER DESIGN

*NO* decimal and binary number should be used directly, use `define` and `parameter` to define constant and parameter.

## BLANK LINE and SPACE

1. insert blank line to enhance readability. among different blocks, continuious assignments, declaration and branch structure.
2. insert space in expression.

## ALIGNMENT and INDENT

1. use `Tab` instead of `Space` to align and indent statements, and `Tab` should be 4 chars wide.
2. `end` must occupy a single line, but `begin` may not
3. when necessary, comment is always a good habit; comment should use single style `//`

## STYLE

1. all the declarations of internal variable should be put together
2. `initial`,`wait`,`while`,`casex`,`casez` should only be used in testbench.
3. ports order: `clocks, resets, control signals, address bus, data bus`