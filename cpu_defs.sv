///////////////////////////////////////////////////////////////////////////////
// Design unit  : cpu_defs
// File name    : cpu_defs.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This module is a package containing a set of public
//				  definitions which could be imported by anu unit
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver1.0 01-March-2015
///////////////////////////////////////////////////////////////////////////////

package cpu_defs;

// instruction parameters
parameter WORD_W		= 32;	// data width
parameter OP_W			= 6;	// opcode width
parameter FUNC_W		= 6;	// functcode width
parameter RF_ADDR_W		= 5;	// register file address width
parameter ALU_CTRL_W	= 4;	// ALU control signal width

// opcodes
enum logic [OP_W-1 : 0] {
	SPECIAL	= 6'b000_000,
	REGIMM	= 6'b000_001,	// BAL, BGEZ, BGEZAL, BLTZ, BLTZAL
	J		= 6'b000_010,
	JAL		= 6'b000_011,
	BEQ		= 6'b000_100,	// B, BEQ,
	BNE		= 6'b000_101,
	BLEZ	= 6'b000_110,
	BGTZ	= 6'b000_111,

	ADDI	= 6'b001_000,
	ADDIU	= 6'b001_001,
	SLTI	= 6'b001_010,
	SLTIU	= 6'b001_011,
	ANDI	= 6'b001_100,
	ORI		= 6'b001_101,
	XORI	= 6'b001_110,
	LUI		= 6'b001_111,

	LB		= 6'b100_000,
	LH		= 6'b100_001,
	LWL		= 6'b100_010,
	LW		= 6'b100_011,
	LBU		= 6'b100_100,
	LHU		= 6'b100_101,
	LWR		= 6'b100_110,

	SB		= 6'b101_000,
	SH		= 6'b101_001,
	SWL		= 6'b101_010,
	SW		= 6'b101_011,
	SWR		= 6'b101_110,
//	CACHE	= 6'b101_111,

	LL		= 6'b110_000,
	SC 		= 6'b111_000
} op_code;

// fucntcodes
enum logic [FUNC_W-1 : 0] {
	SLL		= 6'b000_000,
	SRL		= 6'b000_010,
	SRA		= 6'b000_011,
	SLLV	= 6'b000_100,
	SRLV	= 6'b000_110,
	SRAV	= 6'b000_111,

	JR		= 6'b001_000,
	JALR	= 6'b001_001,
	MOVZ	= 6'b001_010,
	MOVN	= 6'b001_011,
//	SYSCALL	= 6'b001_100,
//	BREAK	= 6'b001_101,
//	SYNC	= 6'b001_111,

	MFHI	= 6'b010_000,
	MTHI	= 6'b010_001,
	MFLO	= 6'b010_010,
	MTLO	= 6'b010_011,

	ADD 	= 6'b100_000,
	ADDU	= 6'b100_001,
	SUB		= 6'b100_010,
	SUBU	= 6'b100_011,
	AND		= 6'b100_100,
	OR		= 6'b100_101,
	XOR		= 6'b100_110,
	NOR		= 6'b100_111,

	SLT		= 6'b101_010,
	SLTU	= 6'b101_011
} funct_code;

// ALU control
enum logic [ALU_CTRL_W-1 : 0] {
	FAND 	= 4'b0000,
	FOR  	= 4'b0001,
	FADD 	= 4'b0010,
	FSUB 	= 4'b0011,
	FXOR 	= 4'b0100,
	FNOR 	= 4'b0101,
	FSLL 	= 4'b0110,
	FSRL 	= 4'b0111
} alu_code;

endpackage