///////////////////////////////////////////////////////////////////////////////
// Design unit  : control
// File name    : control.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This module is control unit which decodes instruction
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver1.0 04-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module control (cpu_bus.control_port bus);

logic [9:0] ctrl;
// package all output ports
assign {
	bus.ctrl_RegWrite_D		,
	bus.ctrl_MemtoReg_D		,
	bus.ctrl_MemWrite_D		,
	bus.ctrl_ALUControl_D	,
	bus.ctrl_ALUSrc_D 		,
	bus.ctrl_RegDst_D 		,
	bus.ctrl_Branch_D
	} = ctrl;

always_comb		begin
	ctrl = 0;
	case (bus.ctrl_Op)
		SPECIAL	:
			case (bus.ctrl_Funct)

				// SLL		: ctrl = 10'b;
				// SRL		: ctrl = 10'b;
				// SRA		: ctrl = 10'b;
				// SLLV	: ctrl = 10'b;
				// SRLV	: ctrl = 10'b;
				// SRAV	: ctrl = 10'b;
				// JR		: ctrl = 10'b;
				// JALR	: ctrl = 10'b;
				// MOVZ	: ctrl = 10'b;
				// MOVN	: ctrl = 10'b;
				// MFHI	: ctrl = 10'b;
				// MTHI	: ctrl = 10'b;
				// MFLO	: ctrl = 10'b;
				// MTLO	: ctrl = 10'b;
				ADD		: ctrl = 10'b100_0010_010;
				ADDU	: ctrl = 10'b100_0010_010;
				SUB		: ctrl = 10'b100_0011_010;
				SUBU	: ctrl = 10'b100_0011_010;
				AND		: ctrl = 10'b100_0000_010;
				OR		: ctrl = 10'b100_0001_010;
				XOR		: ctrl = 10'b100_0100_010;
				NOR		: ctrl = 10'b100_0101_010;
				// SLT		: ctrl = 10'b;
				// SLTU	: ctrl = 10'b;
				default : ;
			endcase

		// REGIMM	: ctrl = 10'b;
		// J		: ctrl = 10'b;
		// JAL		: ctrl = 10'b;
		BEQ		: ctrl = 10'b000_0011_001;	//FSUB
		// BNE		: ctrl = 10'b;
		// BLEZ	: ctrl = 10'b;
		// BGTZ	: ctrl = 10'b;
		// ADDI	: ctrl = 10'b;
		// ADDIU	: ctrl = 10'b;
		// SLTI	: ctrl = 10'b;
		// SLTIU	: ctrl = 10'b;
		// ANDI	: ctrl = 10'b;
		// ORI		: ctrl = 10'b;
		// XORI	: ctrl = 10'b;
		// LUI		: ctrl = 10'b;
		// LB		: ctrl = 10'b;
		// LH		: ctrl = 10'b;
		// LWL		: ctrl = 10'b;
		LW		: ctrl = 10'b110_0010_100; 	//FADD   
		// LBU		: ctrl = 10'b;
		// LHU		: ctrl = 10'b;
		// LWR		: ctrl = 10'b;
		// SB		: ctrl = 10'b;
		// SH		: ctrl = 10'b;
		// SWL		: ctrl = 10'b;
		SW		: ctrl = 10'b001_0010_100;	//FADD
		// SWR		: ctrl = 10'b;
		// LL		: ctrl = 10'b;
		// SC		: ctrl = 10'b;
		default : ;
	endcase
end
endmodule
