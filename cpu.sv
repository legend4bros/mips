///////////////////////////////////////////////////////////////////////////////
// Design unit  : cpu
// File name    : cpu.sv
// Project name	: MIPS
// Top level	:
// Description  : This module is cpu top level
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver1.0 04-March-2015
///////////////////////////////////////////////////////////////////////////////

`include "cpu_defs.sv"
`include "cpu_bus.sv"

`include "IM.sv"
`include "DM.sv"
`include "RF.sv"
`include "ALU.sv"
`include "datapath.sv"
`include "control.sv"
`include "hazard.sv"

import cpu_defs::*;

module cpu (input logic clk, rst_n);

	cpu_bus		bus 		(.*);

	IM 			im0			(.*);
	DM 			dm0			(.*);
	RF 			rf0			(.*);
	ALU 		alu0		(.*);
	datapath 	datapath0	(.*);
	control 	control0	(.*);
	hazard 		hazard0		(.*);

endmodule
