`include "cpu.sv"

module cpu_stim;

bit clk, rst_n;

cpu cpu0 (.*);

always begin
	clk = 0; #5;
	clk = 1; #5;
end

initial
begin
	rst_n = '1;
	#1 rst_n = '0;
	#1 rst_n = '1;
	cpu0.rf0.REG[16] = 0;
	cpu0.rf0.REG[18] = 4;
	cpu0.rf0.REG[19] = 16;
	cpu0.rf0.REG[17] = 16;
	cpu0.rf0.REG[20] =  8;
	cpu0.rf0.REG[21] = 4;
	
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	$stop;
end

endmodule


