///////////////////////////////////////////////////////////////////////////////
// Design unit  : hazard
// File name    : hazard.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This is hazard unit solving hazard raising by pipeline
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver3.0 09-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module hazard (cpu_bus.hazard_port bus);

logic	lwstall, branchstall;

assign	bus.ctrl_ForwardA_E = (bus.ctrl_RegWrite_M && bus.addr_Rs_E != 0 && bus.addr_Rs_E == bus.addr_WriteReg_M) ? 2'b10 : ((bus.ctrl_RegWrite_W && bus.addr_Rs_E != 0 && bus.addr_Rs_E == bus.addr_WriteReg_W) ? 2'b01 : 2'b00);
assign	bus.ctrl_ForwardB_E = (bus.ctrl_RegWrite_M && bus.addr_Rt_E != 0 && bus.addr_Rt_E == bus.addr_WriteReg_M) ? 2'b10 : ((bus.ctrl_RegWrite_W && bus.addr_Rt_E != 0 && bus.addr_Rt_E == bus.addr_WriteReg_W) ? 2'b01 : 2'b00);

assign	bus.ctrl_ForwardA_D = bus.ctrl_RegWrite_M && (bus.addr_Rs_D != '0) && (bus.addr_Rs_D == bus.addr_WriteReg_M);
assign	bus.ctrl_ForwardB_D = bus.ctrl_RegWrite_M && (bus.addr_Rt_D != '0) && (bus.addr_Rt_D == bus.addr_WriteReg_M);

assign	lwstall		= bus.ctrl_MemtoReg_E && (bus.addr_Rs_D == bus.addr_Rt_E || bus.addr_Rt_D == bus.addr_Rt_E);
assign	branchstall	= (bus.ctrl_Branch_D && bus.ctrl_RegWrite_E && (bus.addr_WriteReg_E == bus.addr_Rs_D || bus.addr_WriteReg_E == bus.addr_Rt_D)) || (bus.ctrl_Branch_D && bus.ctrl_MemtoReg_M && (bus.addr_WriteReg_M == bus.addr_Rs_D || bus.addr_WriteReg_M ==bus.addr_Rt_D));

assign	{bus.ctrl_Stall_F, bus.ctrl_stall_D, bus.ctrl_Flush_E} = {3{lwstall || branchstall}};

endmodule
