///////////////////////////////////////////////////////////////////////////////
// Design unit  : cpu_bus
// File name    : cpu_bus.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This is interface shared by all units
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver3.0 09-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

interface cpu_bus (input logic clk, rst_n);

////////////////// control ports/////////////////

logic					ctrl_RegWrite_D	,
						ctrl_MemtoReg_D	,
						ctrl_MemWrite_D	,
						ctrl_ALUSrc_D	,
						ctrl_RegDst_D	,
						ctrl_Branch_D	,
						ctrl_Stall_F	,
						ctrl_stall_D	,
						ctrl_ForwardA_D	,
						ctrl_ForwardB_D	,
						ctrl_Flush_E	,
						ctrl_MemtoReg_E	,
						ctrl_RegWrite_E	,
						ctrl_MemtoReg_M	,
						ctrl_RegWrite_M	,
						ctrl_RegWrite_W	,
						ctrl_DM_WE		,
						ctrl_RF_WE3		;

logic [1:0]				ctrl_ForwardA_E	,
						ctrl_ForwardB_E	;

logic [ALU_CTRL_W-1:0]	ctrl_ALUControl_D,
						ctrl_ALUControl_E;

logic [OP_W-1:0]		ctrl_Op			;
logic [FUNC_W-1:0]		ctrl_Funct		;

////////////////// address ports/////////////////

logic [WORD_W-1:0]		addr_IM_A		,
						addr_DM_A		;

logic [RF_ADDR_W-1:0]	addr_RF_A1		,
						addr_RF_A2		,
						addr_RF_A3		,
						addr_Rs_D		,
						addr_Rt_D		,
						addr_Rs_E		,
						addr_Rt_E		,
						addr_WriteReg_E	,
						addr_WriteReg_M	,
						addr_WriteReg_W	;

////////////////// data ports/////////////////

logic [WORD_W-1:0]		data_IM_RD		,
						data_RF_RD1		,
						data_RF_RD2		,
						data_DM_RD		,
						data_RF_WD3		,
						data_DM_WD		,
						data_SrcA_E		,
						data_SrcB_E		,
						data_ALUOut_E	;

logic					data_ZFlag_E	;

/////////////////// modports defination ////////////////

modport ALU_port (
	output	// to datapath
			data_ALUOut_E		,
			data_ZFlag_E		,
	input	// from datapath
			ctrl_ALUControl_E	,
			data_SrcA_E			,
			data_SrcB_E
	);

modport IM_port (
	output	// to datapath
			data_IM_RD			,
	input	// from datapath
			addr_IM_A
	);

modport RF_port (
	output	// to datapath
			data_RF_RD1			,
			data_RF_RD2			,
	input	clk					,
			// from datapath
			ctrl_RF_WE3			,
			addr_RF_A1			,
			addr_RF_A2			,
			addr_RF_A3			,
			data_RF_WD3
	);

modport DM_port (
	output	// to datapath
			data_DM_RD			,
	input	clk					,
			// from datapath
			ctrl_DM_WE			,
			addr_DM_A			,
			data_DM_WD
	);

modport datapath_port (
	output	// to IM
			addr_IM_A			,
			// to RF
			ctrl_RF_WE3			,
			addr_RF_A1			,
			addr_RF_A2			,
			addr_RF_A3			,
			data_RF_WD3			,
			// to DM
			ctrl_DM_WE			,
			addr_DM_A			,
			data_DM_WD			,
			// to ALU
			ctrl_ALUControl_E	,
			data_SrcA_E			,
			data_SrcB_E			,
			// to control
			ctrl_Op				,
			ctrl_Funct			,
			// to hazard
			ctrl_RegWrite_W			,
			ctrl_RegWrite_M			,
			ctrl_RegWrite_E			,
			ctrl_MemtoReg_M			,
			ctrl_MemtoReg_E			,
			addr_Rs_D			,
			addr_Rt_D			,
			addr_Rs_E			,
			addr_Rt_E 			,
			addr_WriteReg_E		,
			addr_WriteReg_M		,
			addr_WriteReg_W		,


	input	clk					,
			rst_n				,
			// from IM
			data_IM_RD			,
			// from RF
			data_RF_RD1			,
			data_RF_RD2			,
			// from DM
			data_DM_RD			,
			// from ALU
			data_ALUOut_E		,
			data_ZFlag_E		,
			// from control unit
			ctrl_RegWrite_D		,
			ctrl_MemtoReg_D		,
			ctrl_MemWrite_D		,
			ctrl_ALUControl_D	,
			ctrl_ALUSrc_D		,
			ctrl_RegDst_D		,
			ctrl_Branch_D		,
			// from hazard unit
			ctrl_Stall_F		,
			ctrl_stall_D		,
			ctrl_ForwardA_D		,
			ctrl_ForwardB_D		,
			ctrl_ForwardA_E		,
			ctrl_ForwardB_E		,
			ctrl_Flush_E
	);

modport control_port (
	output	// to datapath
			ctrl_RegWrite_D		,
			ctrl_MemtoReg_D		,
			ctrl_MemWrite_D		,
			ctrl_ALUControl_D	,
			ctrl_ALUSrc_D 		,
			ctrl_RegDst_D		,
			ctrl_Branch_D		, // also to hazard
	input	// from datapath
			ctrl_Op 			,
			ctrl_Funct
	);

modport hazard_port (
	output	// to datapath
			ctrl_Stall_F		,
			ctrl_stall_D		,
			ctrl_ForwardA_D		,
			ctrl_ForwardB_D		,
			ctrl_ForwardA_E		,
			ctrl_ForwardB_E		,
			ctrl_Flush_E		,
	input	// from datapath
			ctrl_MemtoReg_E		,
			ctrl_RegWrite_E		,
			ctrl_MemtoReg_M		,
			ctrl_RegWrite_M		,
			ctrl_RegWrite_W		,
			addr_Rs_D			,
			addr_Rt_D			,
			addr_Rs_E			,
			addr_Rt_E 			,
			addr_WriteReg_E		,
			addr_WriteReg_M		,
			addr_WriteReg_W		,
			// from control
			ctrl_Branch_D
	);
// modport cpu_port (
// 	input	clk					,
// 			rst_n
// 	);

endinterface
