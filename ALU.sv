///////////////////////////////////////////////////////////////////////////////
// Design unit  : ALU
// File name    : ALU.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : general ALU
// Author       : zc1g14@ecs.soton.ac.uk
// Revisor		: ww6g14@ecs.soton.ac.uk
// Release      : Ver2.1 03-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module ALU (cpu_bus.ALU_port bus);

always_comb begin
	bus.data_ZFlag_E = '0;
	bus.data_ALUOut_E = '0;

	case (bus.ctrl_ALUControl_E)
		FAND	: bus.data_ALUOut_E = bus.data_SrcA_E & bus.data_SrcB_E;
		FOR		: bus.data_ALUOut_E = bus.data_SrcA_E | bus.data_SrcB_E;
		FADD	: bus.data_ALUOut_E = bus.data_SrcA_E + bus.data_SrcB_E;
		FSUB	: begin
					bus.data_ALUOut_E = bus.data_SrcA_E + ~bus.data_SrcB_E + 1;
					bus.data_ZFlag_E = (bus.data_SrcA_E == bus.data_SrcB_E);
			  		end
		FXOR	: bus.data_ALUOut_E = bus.data_SrcA_E ^ bus.data_SrcB_E;
		FNOR	: bus.data_ALUOut_E = ~(bus.data_SrcA_E | bus.data_SrcB_E);
		FSLL	: bus.data_ALUOut_E = bus.data_SrcA_E << bus.data_SrcB_E;
		FSRL	: bus.data_ALUOut_E = bus.data_SrcA_E >> bus.data_SrcB_E;
		default  ;
	endcase
end

endmodule
