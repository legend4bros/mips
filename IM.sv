///////////////////////////////////////////////////////////////////////////////
// Design unit  : IM
// File name    : IM.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This module is memory where instruction could be read from
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver2.1 03-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module IM #(parameter MEM_SIZE = 64) (cpu_bus.IM_port bus);

logic [WORD_W-1:0] RAM[MEM_SIZE-1:0];

initial
	$readmemb("memfile.dat", RAM);

assign bus.data_IM_RD = RAM[bus.addr_IM_A[WORD_W-1:2]];

endmodule