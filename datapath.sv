///////////////////////////////////////////////////////////////////////////////
// Design unit  : datapath
// File name    : datapath.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This module is datapath
// Author       : ww6g14@ecs.soton.ac.uk
// Revisor	: zc1g14@ecs.soton.ac.uk
// Release      : Ver3.0 09-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module datapath (cpu_bus.datapath_port bus);
/////////////// internal signals //////////////
logic [WORD_W-1:0]		addr_PC_F			,
						addr_PCPlus4_F		,
						addr_PCPlus4_D		,
						addr_PCBranch_D		,
						data_Instr_D		,
						data_SignImm_D		,
						data_RegA_D			,
						data_RegB_D			,
						data_SignImm_E		,
						data_WriteData_E	,
						data_RegA_E			,
						data_RegB_E			,
						data_ALUOut_M		,
						data_WriteData_M	,
						data_ReadData_W		,
						data_ALUOut_W		,
						data_Result_W		;

logic [RF_ADDR_W-1:0]	addr_Rs_D			,
						addr_Rt_D			,
						addr_Rd_D			,
						addr_Rs_E			,
						addr_Rt_E			,
						addr_Rd_E			,
						addr_WriteReg_E		,
						addr_WriteReg_M		,
						addr_WriteReg_W		;

logic					ctrl_PCSrc_D		,
						ctrl_Equal_D		,
						//ctrl_RegWrite_E		,
						//ctrl_MemtoReg_E		,
						ctrl_MemWrite_E		,
						ctrl_ALUSrc_E		,
						ctrl_RegDst_E		,
						//ctrl_RegWrite_M		,
						//ctrl_MemtoReg_M		,
						ctrl_MemWrite_M		,
						//ctrl_RegWrite_W		,
						ctrl_MemtoReg_W		;

logic [ALU_CTRL_W-1:0]	ctrl_ALUControl_E	;

/////////////// comb logic //////////////////

// output logic
assign	bus.addr_IM_A 			= addr_PC_F;
assign	bus.ctrl_RF_WE3 		= bus.ctrl_RegWrite_W;
assign	bus.addr_RF_A1 			= data_Instr_D[25:21];
assign	bus.addr_RF_A2 			= data_Instr_D[20:16];
assign	bus.addr_RF_A3 			= addr_WriteReg_W;
assign	bus.data_RF_WD3 		= data_Result_W;
assign	bus.ctrl_DM_WE 			= ctrl_MemWrite_M;
assign	bus.addr_DM_A 			= data_ALUOut_M;
assign	bus.data_DM_WD 			= data_WriteData_M;
assign	bus.data_SrcA_E 		= bus.ctrl_ForwardA_E[1] ? data_ALUOut_M : (bus.ctrl_ForwardA_E[0] ? data_Result_W : data_RegA_E);
assign	bus.data_SrcB_E 		= ctrl_ALUSrc_E ? data_SignImm_E : data_WriteData_E;
assign	bus.ctrl_Op 			= data_Instr_D[31:26];
assign	bus.ctrl_Funct 			= data_Instr_D[5:0];
assign	bus.addr_Rs_D 			= addr_Rs_D;
assign	bus.addr_Rt_D 			= addr_Rt_D;
assign	bus.addr_Rs_E 			= addr_Rs_E;
assign	bus.addr_Rt_E 			= addr_Rt_E;
assign	bus.addr_WriteReg_E 	= addr_WriteReg_E;
assign	bus.addr_WriteReg_M 	= addr_WriteReg_M;
assign	bus.addr_WriteReg_W 	= addr_WriteReg_W;
assign	bus.ctrl_ALUControl_E 	= ctrl_ALUControl_E;
// internal logic
assign	data_RegA_D				= bus.ctrl_ForwardA_D ? data_ALUOut_M : bus.data_RF_RD1;
assign	data_RegB_D				= bus.ctrl_ForwardB_D ? data_ALUOut_M : bus.data_RF_RD2;
assign	ctrl_Equal_D			= data_RegA_D == data_RegB_D;
assign	ctrl_PCSrc_D			= bus.ctrl_Branch_D & ctrl_Equal_D;
assign	addr_PCPlus4_F			= addr_PC_F + 4;
assign	data_SignImm_D			= {{16{data_Instr_D[15]}}, data_Instr_D[15:0]};
assign	addr_PCBranch_D			= (data_SignImm_D << 2) +  addr_PCPlus4_D;
assign	data_Result_W			= ctrl_MemtoReg_W ? data_ReadData_W : data_ALUOut_W;
assign	data_WriteData_E		= bus.ctrl_ForwardB_E[1] ? data_ALUOut_M : (bus.ctrl_ForwardB_E[0] ? data_Result_W : data_RegB_E);
assign	addr_WriteReg_E 		= ctrl_RegDst_E ? addr_Rd_E : addr_Rt_E;
assign	addr_Rs_D 				= data_Instr_D[25:21];
assign	addr_Rt_D				= data_Instr_D[20:16];
assign	addr_Rd_D				= data_Instr_D[15:11];

/////////////// sequ logic ///////////////////
always_ff @(posedge bus.clk or negedge bus.rst_n) begin
	// fetch stage
	if(!bus.rst_n)
		addr_PC_F 			<= '0;
	else if(!bus.ctrl_Stall_F)
		addr_PC_F			<= ctrl_PCSrc_D ? addr_PCBranch_D : addr_PCPlus4_F;

	// decode stage
	if(!bus.ctrl_stall_D) begin
		if(ctrl_PCSrc_D) begin
			addr_PCPlus4_D	<= '0;
			data_Instr_D	<= '0;
		end
		else begin
			addr_PCPlus4_D	<= addr_PCPlus4_F;
			data_Instr_D	<= bus.data_IM_RD;
		end
	end

	// execute stage
	if(bus.ctrl_Flush_E) begin
		bus.ctrl_RegWrite_E		<= '0;
		bus.ctrl_MemtoReg_E		<= '0;
		ctrl_MemWrite_E		<= '0;
		data_RegA_E			<= '0;
		data_RegB_E			<= '0;
		ctrl_ALUControl_E 	<= '0;
		ctrl_ALUSrc_E		<= '0;
		data_SignImm_E		<= '0;
		ctrl_RegDst_E		<= '0;
		addr_Rs_E			<= '0;
		addr_Rt_E 			<= '0;
		addr_Rd_E 			<= '0;
	end
	else begin
		bus.ctrl_RegWrite_E		<= bus.ctrl_RegWrite_D;
		bus.ctrl_MemtoReg_E		<= bus.ctrl_MemtoReg_D;
		ctrl_MemWrite_E		<= bus.ctrl_MemWrite_D;
		data_RegA_E			<= bus.data_RF_RD1;
		data_RegB_E			<= bus.data_RF_RD2;
		ctrl_ALUControl_E 	<= bus.ctrl_ALUControl_D;
		ctrl_ALUSrc_E		<= bus.ctrl_ALUSrc_D;
		data_SignImm_E		<= data_SignImm_D;
		ctrl_RegDst_E		<= bus.ctrl_RegDst_D;
		addr_Rs_E			<= addr_Rs_D;
		addr_Rt_E 			<= addr_Rt_D;
		addr_Rd_E 			<= addr_Rd_D;
	end

	// memory stage
	bus.ctrl_RegWrite_M			<= bus.ctrl_RegWrite_E;
	bus.ctrl_MemtoReg_M			<= bus.ctrl_MemtoReg_E;
	data_ALUOut_M			<= bus.data_ALUOut_E;
	ctrl_MemWrite_M			<= ctrl_MemWrite_E;
	data_WriteData_M		<= data_WriteData_E;
	addr_WriteReg_M			<= addr_WriteReg_E;

	// wirte back stage
	bus.ctrl_RegWrite_W			<= bus.ctrl_RegWrite_M;
	ctrl_MemtoReg_W			<= bus.ctrl_MemtoReg_M;
	data_ReadData_W			<= bus.data_DM_RD;
	data_ALUOut_W			<= data_ALUOut_M;
	addr_WriteReg_W			<= addr_WriteReg_M;

end
endmodule
