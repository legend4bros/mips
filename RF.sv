///////////////////////////////////////////////////////////////////////////////
// Design unit  : RF
// File name    : RF.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This module is register file
// Author       : yl38e14@ecs.soton.ac.uk
// Revisor		: ww6g14@ecs.soton.ac.uk
// Release      : Ver2.1 03-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module RF (cpu_bus.RF_port bus);

logic [WORD_W-1 : 0] REG[WORD_W - 1 : 0];

assign bus.data_RF_RD1 = (bus.addr_RF_A1 != 0) ? REG[bus.addr_RF_A1] : '0;
assign bus.data_RF_RD2 = (bus.addr_RF_A2 != 0) ? REG[bus.addr_RF_A2] : '0;

always_ff @(negedge bus.clk)
	if (bus.ctrl_RF_WE3)
		REG[bus.addr_RF_A3] <= bus.data_RF_WD3;

endmodule