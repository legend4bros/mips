///////////////////////////////////////////////////////////////////////////////
// Design unit  : DM
// File name    : DM.sv
// Project name	: MIPS
// Top level	: cpu.sv
// Description  : This module is memory where data could be written into
//				: and read from
// Author       : ww6g14@ecs.soton.ac.uk
// Release      : Ver2.1 03-March-2015
///////////////////////////////////////////////////////////////////////////////

import cpu_defs::*;

module DM #(parameter MEM_SIZE = 64) (cpu_bus.DM_port bus);

logic [WORD_W-1:0] RAM[MEM_SIZE-1:0];

assign bus.data_DM_RD = RAM[bus.addr_DM_A[WORD_W-1:2]];

always_ff @(posedge bus.clk)
	if (bus.ctrl_DM_WE)
		RAM[bus.addr_DM_A[WORD_W-1:2]] <= bus.data_DM_WD;

endmodule